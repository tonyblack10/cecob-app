import { SiginPage } from './signin.po';

describe('Signin', () => {
  let page: SiginPage;

  beforeEach(() => {
    page = new SiginPage();
  });

  it('deve realizar login dados válidos', () => {
    page.visita()
    page.realizaLoginComUsuarioValido();
    expect(page.temMessagem('Bem-vindo!')).toBeTruthy();
  });

});
