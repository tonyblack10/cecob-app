import { browser, by, element, promise } from 'protractor';

export class SiginPage {

  private usuario = { email: 'usuario@email.com', senha: '123456' };

  visita() {
    return browser.get('/');
  }

  realizaLoginComUsuarioValido() {
    const campoEmail = element(by.id('email'));
    const campoSenha = element(by.id('senha'));
    const btnSubmit = element(by.id('btn-login'));
    campoEmail.sendKeys(this.usuario.email);
    campoSenha.sendKeys(this.usuario.senha);
    btnSubmit.click();
  }

  temMessagem(mensagem: string): promise.Promise<boolean> {
    return browser.getPageSource()
      .then(source => 
        source.includes(mensagem) ? true : false);
  } 

}
