import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './core/auth/auth.guard';
import { LoginGuard } from './core/auth/login.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: './home/home.module#HomeModule',
    canActivate: [LoginGuard]
  },
  {
    path: 'dashboard',
    loadChildren: './inicio/inicio.module#InicioModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'usuarios',
    loadChildren: './usuarios/usuarios.module#UsuariosModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'despesas',
    loadChildren: './despesas/despesas.module#DespesasModule',
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
