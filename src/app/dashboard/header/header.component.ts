declare var $;

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';

import { UserService } from './../../core/user/user.service';
import { User } from '../../core/user/user';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'cb-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  user$: Observable<User>;

  constructor(
    private userService: UserService, 
    private router: Router,
    private toastr: ToastrManager
  ) {}

  ngOnInit() {
    this.user$ = this.userService.getUser();
  }

  menuToggle(e) {
    e.preventDefault();
    if (window.matchMedia('(min-width: 1200px)').matches) {
      $('body').toggleClass('hide-sidebar');
    } else {
      $('body').toggleClass('show-sidebar');
    }
  }

  logout() {
    this.userService.logout();
    this.toastr.successToastr('Logout realizado com sucesso.');
    this.router.navigate(['/']);
  }

}
