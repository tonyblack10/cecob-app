import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'username'
})
export class UsernamePipe implements PipeTransform {
  
  transform(value: string, ...args: any[]) {
    if(!value) return '';

    const nomeArr = value.split(' ');
    if(nomeArr.length === 1)
      return nomeArr[0];
    
    return `${nomeArr[0]} ${nomeArr[nomeArr.length - 1]}`;
  }  

}