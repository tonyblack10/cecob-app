import { Component, Input } from '@angular/core';

@Component({
  selector: 'cb-mainpanel',
  templateUrl: './mainpanel.component.html'
})
export class MainpanelComponent {

  @Input() titulo: string;

}
