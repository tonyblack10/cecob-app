declare var $;

import { Component } from '@angular/core';

@Component({
  selector: 'cb-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

  toggleMenu(e) {
    e.preventDefault();
    const subMenu = $(event.target).next();

    $(event.target).parent().siblings().find('.sidebar-nav-sub').slideUp();
    $('.sub-with-sub ul').slideUp();

    if(subMenu.length) {
      e.preventDefault();
      subMenu.slideToggle();
    }
  }

}
