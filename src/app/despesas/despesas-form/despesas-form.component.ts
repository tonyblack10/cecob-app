declare var $;

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ToastrManager } from 'ng6-toastr-notifications';

import { DespesaService } from '../services/despesa.service';
import { TipoDeDespesaService } from '../services/tipo-despesa.service';
import { CredorService } from '../services/credor.service';
import { ElementRef } from '@angular/core';
import { TipoDeDespesa } from '../models/tipo-despesa';
import { Credor } from '../models/credor';
import { Despesa } from '../models/despesa';
import { ModalComponent } from '../../shared/components/modal/modal.component';

@Component({
  templateUrl: './despesas-form.component.html',
  styleUrls: ['./despesas-form.component.css']
})
export class DespesasFormComponent implements OnInit {

  tiposDeDespesas$: Observable<TipoDeDespesa[]>;
  credores$: Observable<Credor[]>;

  @ViewChild('modalAddCredorComponent') modalAddCredorComponent: ModalComponent;
  @ViewChild('modalAddTipoDeDespesaComponent') modalAddTipoDeDespesaComponent: ModalComponent;
  @ViewChild('inputDescricao') inputDescricao: ElementRef<HTMLInputElement>;

  despesaId: number;
  despesaForm: FormGroup;
  credorForm: FormGroup;
  tipoDeDespesaForm: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private despesaService: DespesaService,
    private credorService: CredorService,
    private tipoDeDespesaService: TipoDeDespesaService,
    private toastr: ToastrManager
  ) {}
    
  ngOnInit(): void {
    this.inputDescricao.nativeElement.focus();
    this.credores$ = this.credorService.getTodos();
    this.tiposDeDespesas$ = this.tipoDeDespesaService.getTodos();
    this.despesaId = this.activatedRoute.snapshot.params['id'];
    this._criaForms();
    if(this.despesaId) {
      this._preencheForm();
    }
  }

  salva() {
    const despesa = this.despesaForm.getRawValue() as Despesa;
    if(this.despesaId) {
      this.despesaService.edita(this.despesaId, despesa)
        .subscribe(() => {
          this.toastr.successToastr('Despesa editada com sucesso.');
          this.router.navigate(['/despesas']);
        });
    } else {
      this.despesaService.salva(despesa)
        .subscribe(() => {
          this.toastr.successToastr('Despesa cadastrada com sucesso.');
          this.router.navigate(['/despesas']);
        });
    }
  }

  mudaStatusPagamento() {
    if(this.despesaForm.controls['paga'].value) {
      this.despesaForm.controls['dataDePagamento'].setValidators(Validators.required);
      this.despesaForm.controls['dataDePagamento'].updateValueAndValidity();
      $('.dadosPagamento').show('slow');
    } else {
      this.despesaForm.controls['dataDePagamento'].setValue('');
      this.despesaForm.controls['desconto'].setValue('');
      this.despesaForm.controls['multa'].setValue('');
      this.despesaForm.controls['dataDePagamento'].clearValidators();
      this.despesaForm.controls['dataDePagamento'].markAsUntouched();
      this.despesaForm.controls['desconto'].markAsUntouched();
      this.despesaForm.controls['multa'].markAsUntouched();
      $('.dadosPagamento').hide('slow');
    }
  }

  private _criaForms() {
    this.credorForm = this.fb.group({ descricao: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(200)]]});
    this.tipoDeDespesaForm = this.fb.group({ descricao: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(200)]]});
    this.despesaForm = this.fb.group({
      descricao: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(200)]],
      dataDaCompetencia: ['', [Validators.required]],
      dataDeVencimento: ['', [Validators.required]],
      valor: ['', [Validators.required, Validators.min(1)]],
      paga: [false],
      dataDePagamento: [''],
      desconto: [''],
      multa: [''],
      'credores_id': ['', Validators.required],
      'tipos_despesas_id': ['', Validators.required],
    });
  }

  private _preencheForm() {
    this.despesaService.buscaPorId(this.despesaId)
      .subscribe(despesa => {
        this.despesaForm.patchValue(despesa);
        if(despesa.dataDePagamento) {
          $('.dadosPagamento').show('slow');
        }
      });
  }

  addCredor() {
    const credor = this.credorForm.getRawValue() as Credor;
    this.credorService.salva(credor)
      .subscribe(credor => {
        this.credorForm.patchValue({descricao: ''});
        this.despesaForm.controls['credores_id'].setValue(credor.id);
        this.credores$ = this.credorService.getTodos();
        this.modalAddCredorComponent.fechar();
      });
  }

  addTipoDeDespesa() {
    const tipoDeDespesa = this.tipoDeDespesaForm.getRawValue() as TipoDeDespesa;
    this.tipoDeDespesaService.salva(tipoDeDespesa)
      .subscribe(tipoDeDespesa => {
        this.tipoDeDespesaForm.patchValue({descricao: ''});
        this.despesaForm.controls['tipos_despesas_id'].setValue(tipoDeDespesa.id);
        this.tiposDeDespesas$ = this.tipoDeDespesaService.getTodos();
        this.modalAddTipoDeDespesaComponent.fechar();
      });
  }
}
