import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { ModalModule } from './../../shared/components/modal/modal.module';
import { DespesasFormComponent } from './despesas-form.component';
import { DespesaService } from '../services/despesa.service';
import { InputModule } from './../../shared/components/input/input.module';
import { TipoDeDespesaService } from '../services/tipo-despesa.service';
import { EstadoDeValidacaoModule } from './../../shared/directives/campo-invalido.directive/campo-invalido.module';
import { CredorService } from '../services/credor.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    InputModule,
    CurrencyMaskModule,
    EstadoDeValidacaoModule,
    ModalModule
  ],
  declarations: [
    DespesasFormComponent
  ],
  exports: [
    DespesasFormComponent
  ],
  providers: [
    DespesaService,
    CredorService,
    TipoDeDespesaService
  ]
})
export class DespesasFormModule {}
