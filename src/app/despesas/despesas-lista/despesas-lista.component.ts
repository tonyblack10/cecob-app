import { Component, OnInit, ViewChild } from '@angular/core';

import { ToastrManager } from 'ng6-toastr-notifications';
import { tap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { DespesaService } from '../services/despesa.service';
import { Pagina } from './../../shared/utils/pagina';
import { Despesa } from '../models/despesa';
import { ModalComponent } from './../../shared/components/modal/modal.component';

@Component({
  templateUrl: './despesas-lista.component.html'
})
export class DespesasListaComponent implements OnInit {

  @ViewChild('modalComponent') modalComponent: ModalComponent;

  paginaAtual = 1;
  despesaRemove: Despesa;
  pagina$: Observable<Pagina<Despesa>>;

  constructor(
    private service: DespesaService, 
    private toastr: ToastrManager
  ) {}
  
  ngOnInit(): void {
    this.pagina$ = this.service.getListaPaginada();
  }

  mudaDePagina(numeroDaPagina: number) {
    this.pagina$ = this.service.getListaPaginada(numeroDaPagina-1);
  }

  remove() {
    if(this.despesaRemove) {
      this.pagina$ = this.service.remove(this.despesaRemove.id)
        .pipe(tap(() => {
          this.modalComponent.fechar();
          this.toastr.successToastr('Despesa removida com sucesso.');
        }))
        .pipe(switchMap(() => this.service.getListaPaginada()));
    }
  }
}
