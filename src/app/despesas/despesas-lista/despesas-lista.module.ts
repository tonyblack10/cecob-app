import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';

import { DespesasListaComponent } from './despesas-lista.component';
import { DespesaService } from '../services/despesa.service';
import { ModalModule } from './../../shared/components/modal/modal.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbPaginationModule,
    ModalModule
  ],
  declarations: [
    DespesasListaComponent
  ],
  exports: [
    DespesasListaComponent
  ],
  providers: [
    DespesaService
  ]
})
export class DespesasListaModule {}
