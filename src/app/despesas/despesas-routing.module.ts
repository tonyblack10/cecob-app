import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DespesasListaComponent } from './despesas-lista/despesas-lista.component';
import { DespesasFormComponent } from './despesas-form/despesas-form.component';

const routes: Routes = [
  { path: '', 
    component: DespesasListaComponent,
    data: {
      titulo: 'Despesas'
    }
  },
  { 
    path: 'novo', 
    component: DespesasFormComponent,
    data: {
      titulo: 'Nova Despesa'
    }
  },
  { 
    path: 'edita/:id', 
    component: DespesasFormComponent,
    data: {
      titulo: 'Edita Despesa'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DespesasRoutingModule {}
