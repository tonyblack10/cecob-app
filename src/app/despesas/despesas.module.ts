import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DespesasListaModule } from './despesas-lista/despesas-lista.module';
import { DespesasRoutingModule } from './despesas-routing.module';
import { DespesasFormModule } from './despesas-form/despesas-form.module';

@NgModule({
  imports: [
    CommonModule,
    DespesasListaModule,
    DespesasFormModule,
    DespesasRoutingModule,
  ]
})
export class DespesasModule {}
