export interface DespesaFilter {
  descricao: string;
  dataVencimentoDe: Date;
  dataVencimentoAte: Date;
  valorDe: number;
  valorAte: number;
  status: boolean;
}
