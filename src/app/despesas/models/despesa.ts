import { Credor } from './credor';
import { TipoDeDespesa } from './tipo-despesa';

export interface Despesa {
  id: number;
  descricao: string;
  dataDaCompetencia: Date;
  dataDeVencimento: Date;
  valor: number;
  dataDePagamento: Date;
  desconto: number;
  multa: number;
  observacoes: string;
  credor: Credor;
  tipoDeDespesa: TipoDeDespesa;
}
