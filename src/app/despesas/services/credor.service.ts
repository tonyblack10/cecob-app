import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Credor } from '../models/credor';
import { environment } from '../../../environments/environment';

@Injectable()
export class CredorService {

  constructor(private http: HttpClient) {}

  getTodos(): Observable<Credor[]> {
    return this.http.get<Credor[]>(`${environment.apiUrl}/credores`);
  }

  salva(credor: Credor): Observable<Credor> {
    return this.http.post<Credor>(`${environment.apiUrl}/credores`, credor);
  }
}
