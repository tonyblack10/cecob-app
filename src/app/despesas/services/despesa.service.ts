import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Despesa } from '../models/despesa';
import { environment } from '../../../environments/environment';
import { DespesaFilter } from '../models/despesa.filter';
import { Pagina } from '../../shared/utils/pagina';

@Injectable()
export class DespesaService {

  constructor(private http: HttpClient) {}

  getListaPaginada(pagina: number = 0, filter?: DespesaFilter): Observable<Pagina<Despesa>> {
    const params = new HttpParams()
      .set('pagina', pagina.toString());

    return this.http.get<Pagina<Despesa>>(`${environment.apiUrl}/despesas`, { params });
  }

  salva(despesa: Despesa): Observable<any> {
    return this.http.post(`${environment.apiUrl}/despesas`, despesa);
  }

  edita(id: number, despesa: Despesa): Observable<any> {
    return this.http.put(`${environment.apiUrl}/despesas/${id}`, despesa);
  }

  buscaPorId(id: number): Observable<Despesa> {
    return this.http.get<Despesa>(`${environment.apiUrl}/despesas/${id}`);
  }

  remove(id: number): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/despesas/${id}`);
  }
}
