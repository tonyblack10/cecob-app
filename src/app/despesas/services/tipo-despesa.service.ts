import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { TipoDeDespesa } from '../models/tipo-despesa';
import { environment } from '../../../environments/environment';

@Injectable()
export class TipoDeDespesaService {

  constructor(private http: HttpClient) {}

  getTodos(): Observable<TipoDeDespesa[]> {
    return this.http.get<TipoDeDespesa[]>(`${environment.apiUrl}/tipos-despesas`);
  }

  salva(tipoDeDespesa: TipoDeDespesa): Observable<any> {
    return this.http.post(`${environment.apiUrl}/tipos-despesas`, tipoDeDespesa);
  }
}
