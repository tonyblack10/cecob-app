import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ToastrManager } from 'ng6-toastr-notifications';

import { AuthService } from './../../core/auth/auth.service';

@Component({
  templateUrl: './signin.component.html'
})
export class SigninComponent implements OnInit {
  
  loginForm: FormGroup;
  @ViewChild('inputEmail') inputEmail: ElementRef<HTMLInputElement>;
  
  constructor(
    private fb: FormBuilder,
    private service: AuthService,
    private router: Router,
    private toastr: ToastrManager
  ) {}

  ngOnInit(): void {
    this.inputEmail.nativeElement.focus();
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      senha: ['', Validators.required]
    });
  }

  login() {
    const { email, senha } = this.loginForm.getRawValue();
    this.service.authenticate(email, senha)
      .subscribe(() => {
        this.router.navigate(['/dashboard']);
        this.toastr.successToastr('Bem-vindo!');
      }, err => {
        console.log(err);
        this.toastr.errorToastr('Credenciais inválidas');
      });
  }

}
