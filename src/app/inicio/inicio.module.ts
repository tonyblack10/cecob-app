import { InicioRoutingModule } from './inicio-routing.module';
import { DashboardModule } from './../dashboard/dashboard.module';
import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardModule,
    InicioRoutingModule
  ],
  declarations: [
    InicioComponent
  ]
})
export class InicioModule {}