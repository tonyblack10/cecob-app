import { Component, AfterContentInit, ContentChild } from '@angular/core';
import { FormControlName } from '@angular/forms';
import { Input } from '@angular/core';

@Component({
  selector: 'cb-input',
  templateUrl: './input.component.html'
})
export class InputComponent implements AfterContentInit {
  
  @Input() label: string;
  @Input() id: string;
  @Input() ehObrigatorio: boolean = false;

  input: FormControlName;
  classePadrao: string;

  @ContentChild(FormControlName) control: FormControlName;

  ngAfterContentInit(): void {
    this.input = this.control;
    if(this.input === undefined){
      throw new Error('Esse componente precisa ser usado com uma diretiva formControlName')
    }
  }

}
