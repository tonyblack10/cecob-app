declare var $;

import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cb-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent {

  @Input() modalId: string;
  @Input() modalLabel: string;
  @Input() titulo = 'Confirmação';

  @Output() confirmarEvent = new EventEmitter<any>();

  confirmar(value: any) {
    this.confirmarEvent.emit(value);
  }

  fechar() {
    $(`#${this.modalId}`).modal('hide');
  }
  
}
