import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';
import { FormControlName } from '@angular/forms';

import { temErro } from '../../utils/temInputErro';

@Directive({
  selector: '[estadoDeValidacao]'
})
export class EstadoDeValidacaoDirective {

  constructor(
    private el: ElementRef, 
    private control: FormControlName,
    private renderer: Renderer
  ) {}

  @HostListener('input')
  @HostListener('blur')
  @HostListener('change')
  aplicaEstiloDeValidacao() {
    if(temErro(this.control)) {
      this.renderer.setElementClass(this.el.nativeElement, 'is-valid', false);
      this.renderer.setElementClass(this.el.nativeElement, 'is-invalid', true);
    } else {
      this.renderer.setElementClass(this.el.nativeElement, 'is-invalid', false);
      this.renderer.setElementClass(this.el.nativeElement, 'is-valid', true);
    }
  }
}
