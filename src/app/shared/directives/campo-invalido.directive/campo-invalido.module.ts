import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { EstadoDeValidacaoDirective } from './campo-invalido.directive';

@NgModule({
  imports: [ReactiveFormsModule],
  declarations: [EstadoDeValidacaoDirective],
  exports: [EstadoDeValidacaoDirective]
})
export class EstadoDeValidacaoModule {}