import { FormControl, FormControlName } from '@angular/forms';

export function temErro(control: FormControl | FormControlName): boolean {
  return control.errors && control.touched; 
}
