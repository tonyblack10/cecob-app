import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Usuario } from './usuario';
import { environment } from '../../environments/environment';

@Injectable()
export class UsuarioService {

  constructor(private http: HttpClient) {}

  getTodos(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(`${environment.apiUrl}/usuarios`);
  }

  salva(usuario: Usuario): Observable<any> {
    return this.http.post(`${environment.apiUrl}/usuarios`, usuario);
  }

  edita(id: number, usuario: Usuario): Observable<any> {
    return this.http.put(`${environment.apiUrl}/usuarios/${id}`, usuario);
  }

  buscaPorId(id: number): Observable<Usuario> {
    return this.http.get<Usuario>(`${environment.apiUrl}/usuarios/${id}`);
  }

  remove(id: number): Observable<any> {
    return this.http.delete(`${environment.apiUrl}/usuarios/${id}`);
  }

  alteraStatus(id: number, status: boolean): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/usuarios/${id}/status`, {status});
  }

  alteraSenha(id: number, senhaAtual: string, novaSenha: string): Observable<any> {
    return this.http.patch(`${environment.apiUrl}/usuarios/${id}/senha`, {
      senhaAtual,
      novaSenha
    });
  }

  checkSeUsuarioExiste(email: string) {
    return this.http.get(`${environment.apiUrl}/usuarios/exists/${email}`);
  }
}
