export interface Usuario {
  id?: number;
  nome: string;
  permissao: string;
  email: string;
  senha?: string;
  senhaRepete?: string;
  status: boolean;
}
