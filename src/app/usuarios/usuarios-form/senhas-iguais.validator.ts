import { ValidatorFn, FormGroup } from '@angular/forms';

export const senhasIguaisValidator: ValidatorFn = (formGroup: FormGroup) => {
  const senha = formGroup.get('senha').value;
  const senhaRepetida = formGroup.get('senhaRepetida').value;

  if(!senha || !senhaRepetida) return null;

  return senha == senhaRepetida ? null : { senhasDiferentes: true };
};
