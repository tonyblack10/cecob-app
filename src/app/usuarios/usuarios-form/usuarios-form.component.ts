import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrManager } from 'ng6-toastr-notifications';

import { UsuarioService } from '../usuario.service';
import { Usuario } from '../usuario';
import { temErro } from '../../shared/utils/temInputErro';
import { VerificaSeEmailExisteValidatorService } from './verifica-se-email-existe.validator.service';
import { senhasIguaisValidator } from './senhas-iguais.validator';

@Component({
  templateUrl: './usuarios-form.component.html',
  providers: [VerificaSeEmailExisteValidatorService]
})
export class UsuariosFormComponent implements OnInit {

  @ViewChild('inputNome') inputNome: ElementRef<HTMLInputElement>;

  temErro: Function = temErro;
  usuario: Usuario;
  usuarioId: number;
  usuarioForm: FormGroup;

  constructor(
    private service: UsuarioService,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private verificaSeEmailExisteService: VerificaSeEmailExisteValidatorService,
    private toastr: ToastrManager
  ) {}

  ngOnInit() {
    this.usuarioId = this.activatedRoute.snapshot.params['id'];
    this.inputNome.nativeElement.focus();
    this.usuarioForm = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(200)]],
      permissao: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      senha: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
      senhaRepetida: ['', [Validators.required]]
    });
    this._preencheForm();
  }

  salva() {
    const usuario = this.usuarioForm.getRawValue() as Usuario;
    if(!this.usuarioId) {
      this.service.salva(usuario)
        .subscribe(() => {
          this.toastr.successToastr('Usuário cadastrado com sucesso.');
          this.router.navigate(['/usuarios']);
        });
    } else {
      this.service.edita(this.usuarioId, usuario)
        .subscribe(() => {
          this.toastr.successToastr('Usuário editado com sucesso.');
          this.router.navigate(['/usuarios']);
        });
    }
  }

  private _preencheForm() {
    if(this.usuarioId) {
      this.service.buscaPorId(this.usuarioId)
        .subscribe(usuario => {
          this.usuario = usuario;
          this.usuarioForm.controls['nome'].setValue(usuario.nome);
          this.usuarioForm.controls['email'].setValue(usuario.email);
          this.usuarioForm.controls['permissao'].setValue(usuario.permissao);
          this.usuarioForm.removeControl('senha');
          this.usuarioForm.removeControl('senhaRepetida');
          this.usuarioForm.controls['email']
            .setAsyncValidators(this.verificaSeEmailExisteService
              .verificaSeEmailExiste(usuario.email)
            )
        });
    } else {
      this._addValidators();
    }
  }

  private _addValidators() {
    this.usuarioForm.setValidators(senhasIguaisValidator);
    this.usuarioForm.controls['email']
      .setAsyncValidators(this.verificaSeEmailExisteService
        .verificaSeEmailExiste('')
      );
  }
}
