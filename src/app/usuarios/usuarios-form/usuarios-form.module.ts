import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UsuariosFormComponent } from './usuarios-form.component';
import { UsuarioService } from '../usuario.service';
import { InputModule } from '../../shared/components/input/input.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    InputModule
  ],
  declarations: [
    UsuariosFormComponent
  ],
  exports: [
    UsuariosFormComponent
  ],
  providers: [
    UsuarioService
  ]
})
export class UsuariosFormModule {}
