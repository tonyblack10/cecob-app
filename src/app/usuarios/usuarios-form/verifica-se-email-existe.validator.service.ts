import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { of } from 'rxjs';
import { debounceTime, switchMap, map, first, tap } from 'rxjs/operators';

import { UsuarioService } from './../usuario.service';

@Injectable()
export class VerificaSeEmailExisteValidatorService {

  constructor(private service: UsuarioService) {}

  verificaSeEmailExiste(emailAtual: string) {
    return (control: AbstractControl) => {
      if(control.hasError('email')) 
        return of(null);
      if(control.value === emailAtual)
        return of(null);

      return control
        .valueChanges
        .pipe(debounceTime(300))
        .pipe(switchMap(email => 
            this.service.checkSeUsuarioExiste(email)
        ))
        .pipe(map((res: any) => 
          res.existe ? { emailExiste: true } : null
        ))
        .pipe(first());
    }
  }
}
