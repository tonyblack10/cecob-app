declare var $;

import { switchMap, tap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { ToastrManager } from 'ng6-toastr-notifications';

import { UsuarioService } from './../usuario.service';
import { Usuario } from '../usuario';

@Component({
  templateUrl: './usuarios-lista.component.html'
})
export class UsuariosListaComponent implements OnInit {

  usuarioRemove: Usuario;
  usuarios$: Observable<Usuario[]>;

  constructor(
    private service: UsuarioService, 
    private toastr: ToastrManager
  ) {}

  ngOnInit() {
    this.usuarios$ = this.service.getTodos();
  }

  remove() {
    if(this.usuarioRemove) {
      this.usuarios$ = this.service.remove(this.usuarioRemove.id)
        .pipe(tap(() => {
          $('#exampleModal').modal('hide');
          this.toastr.successToastr('Usuário removido com sucesso.');
        }))
        .pipe(switchMap(() => this.service.getTodos()));
    }
  }
}
