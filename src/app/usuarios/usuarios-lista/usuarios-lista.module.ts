import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { UsuariosListaComponent } from './usuarios-lista.component';
import { UsuarioService } from '../usuario.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    UsuariosListaComponent
  ],
  exports: [
    UsuariosListaComponent
  ],
  providers: [
    UsuarioService
  ]
})
export class UsuariosListaModule {}
