import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsuariosListaComponent } from './usuarios-lista/usuarios-lista.component';
import { UsuariosFormComponent } from './usuarios-form/usuarios-form.component';

const routes: Routes = [
  { 
    path: '', 
    component: UsuariosListaComponent,
    data: {
      titulo: 'Usuários'
    }
  },
  { 
    path: 'novo', 
    component: UsuariosFormComponent,
    data: {
      titulo: 'Novo Usuário'
    }
  },
  { 
    path: 'edita/:id', 
    component: UsuariosFormComponent,
    data: {
      titulo: 'Edita Usuário'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UsuariosRoutingModule {}
