import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosListaModule } from './usuarios-lista/usuarios-lista.module';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosFormModule } from './usuarios-form/usuarios-form.module';

@NgModule({
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    UsuariosListaModule,
    UsuariosFormModule
  ]
})
export class UsuariosModule {}
